Information
-----------
See the [fractal wiki](https://bitbucket.org/aschaller/fractal-antenna-generator/wiki/Home) for details on what the purpose of this repository is. If you would like to view images of rendered antennas, you can visit the [Output/](https://bitbucket.org/aschaller/fractal-antenna-generator/src/7aec04a7c9de/Output) folder. The primary script that generates the fractal antennas is located in [4NEC2/4NEC2_Generator.py](https://bitbucket.org/aschaller/fractal-antenna-generator/src/7aec04a7c9de/4NEC2/4NEC2_Generator.py)


Installation
------------
Let me guess, you're probably anxious to try this program out for yourself, right? Here's how you can do it:


1.  Install [Python 2.7](http://python.org/download/) - this is the language the `4NEC2_Generator.py` code was written in, in addition to the other sub-directories in this repository.
2.  Install [4NEC2](http://home.ict.nl/~arivoors/) - this is the actual antenna modeler and optimizer. You can twirl your antenna three-dimensionally, examine SWR vs. Frequency plots, and a lot more.
3.  If you don't want to install Git, I would advise you to simply download this repository to instantly get started. Otherwise, do a git pull. - this allows you to have a copy of my repository on your machine and experiment with its capabililties.


Now that you have everything installed you can start rendering fractal antennas! Activate IDLE (part of Python) and bring the program to life by opening up `4NEC2_Generator.py` in the `4NEC2` folder. Press F5 or "run" to initiate the routine.

Enjoy!
		
		

License
-------

Copyright (c) 2011 Austin R. Schaller <schaller.austin@gmail.com>     
Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute,  and/or grant sublicense copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
the following conditions:
        
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.      
        
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



Example Rendered Antenna
------------------------

![Hilbert Curve](https://bitbucket.org/aschaller/fractal-antenna-generator/raw/be26e9bbd60e/Output/HilbertCurve_5thIteration_Test/HilbertCurve_0001_3D_RadiationPattern.png)